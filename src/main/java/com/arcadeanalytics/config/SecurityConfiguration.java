package com.arcadeanalytics.config;

/*-
 * #%L
 * Arcade Analytics
 * %%
 * Copyright (C) 2018 - 2019 ArcadeAnalytics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.Optional;

import com.arcadeanalytics.domain.User;
import com.arcadeanalytics.domain.enumeration.ContractType;
import com.arcadeanalytics.repository.UserRepository;
import com.arcadeanalytics.security.AuthoritiesConstants;
import com.arcadeanalytics.security.jwt.JWTConfigurer;
import com.arcadeanalytics.security.jwt.TokenProvider;
import com.arcadeanalytics.service.UserService;
import com.arcadeanalytics.service.dto.UserDTO;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import javax.annotation.PostConstruct;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

@Configuration
@Import(SecurityProblemSupport.class)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final CacheManager cacheManager;

    private final UserDetailsService userDetailsService;

    private final TokenProvider tokenProvider;

    private final CorsFilter corsFilter;

    private final SecurityProblemSupport problemSupport;
    private final ApplicationProperties applicationProperties;

    public SecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder,
                                 CacheManager cacheManager,
                                 UserDetailsService userDetailsService,
                                 TokenProvider tokenProvider,
                                 CorsFilter corsFilter,
                                 SecurityProblemSupport problemSupport,
                                 ApplicationProperties applicationProperties) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.cacheManager = cacheManager;
        this.userDetailsService = userDetailsService;
        this.tokenProvider = tokenProvider;
        this.corsFilter = corsFilter;
        this.problemSupport = problemSupport;
        this.applicationProperties = applicationProperties;
    }

    @PostConstruct
    public void init() {
        try {
            final PreAuthenticatedAuthenticationProvider preAuth = getApplicationContext().getBean(PreAuthenticatedAuthenticationProvider.class);
            authenticationManagerBuilder
                    .parentAuthenticationManager(preAuth::authenticate)
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }

    @Bean
    public RequestHeaderAuthenticationFilter oauth2ProxyHeaderFilter(AuthenticationManager authenticationManager) {
        RequestHeaderAuthenticationFilter result = new RequestHeaderAuthenticationFilter();
        result.setPrincipalRequestHeader(applicationProperties.getPrincipalRequestHeader());
        // result.setCredentialsRequestHeader(applicationProperties.getCredentialRequestHeader());
        result.setExceptionIfHeaderMissing(false);
        result.setAuthenticationManager(authenticationManager);
        return result;
    }

    @Bean
    public PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider(UserService userService) {
        PreAuthenticatedAuthenticationProvider result = new PreAuthenticatedAuthenticationProvider();
        result.setOrder(Ordered.HIGHEST_PRECEDENCE);
        result.setPreAuthenticatedUserDetailsService(token -> {
            final String username = (String) token.getPrincipal();
            final String login = username.toLowerCase();
            Optional<User> maybeUser = Optional.ofNullable(
                    cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).get(login)
            ).map(Cache.ValueWrapper::get).map(User.class::cast);
            if (!maybeUser.isPresent()) {
                maybeUser = userService.getUserWithAuthoritiesByLogin(login);
            }
            final User user = maybeUser
                    .orElseGet(() -> {
                        UserDTO dto = new UserDTO();
                        dto.setLogin(login);
                        dto.setEmail(username);
                        dto.setActivated(true);
                        dto.setContractType(ContractType.GOLD.name());
                        dto.setAuthorities(new HashSet<>(asList(AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER)));
                        return userService.createUser(dto);
                    });
            return new org.springframework.security.core.userdetails.User(
                    login,
                    user.getPassword(),
                    user.getAuthorities().stream()
                            .map(a -> new SimpleGrantedAuthority(a.getName()))
                            .collect(toSet()));
        });
        result.setThrowExceptionWhenTokenRejected(true);
        // result.setUserDetailsChecker();
        return result;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) {
        requireNonNull(web, "Unable to configure a NULL WebSecurity").ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/favicon.ico")
                .antMatchers("/app/**/*.{js,html}")
                .antMatchers("/i18n/**")
                .antMatchers("/content/**")
                .antMatchers("/swagger-ui/index.html")
                .antMatchers("/test/**")
                .antMatchers("/h2-console/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        final RequestHeaderAuthenticationFilter headerFilter = this.getApplicationContext().getBean(RequestHeaderAuthenticationFilter.class);
        http
                .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(headerFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(problemSupport)
                .accessDeniedHandler(problemSupport)
                .and()
                .csrf()
                .disable()
                .headers()
                .frameOptions()
                .disable()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/register").permitAll()
                .antMatchers("/api/activate").permitAll()
                .antMatchers("/api/authenticate").permitAll()
                .antMatchers("/api/account/reset-password/init").permitAll()
                .antMatchers("/api/account/reset-password/finish").permitAll()
                .antMatchers("/api/profile-info").permitAll()
                .antMatchers("/api/embed/**").permitAll()
                .antMatchers("/api/**").authenticated()
                .antMatchers("/websocket/tracker").hasAuthority(AuthoritiesConstants.ADMIN)
                .antMatchers("/websocket/**").permitAll()
                .antMatchers("/management/health").permitAll()
                .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
                .antMatchers("/v2/api-docs/**").permitAll()
                .antMatchers("/swagger-resources/configuration/ui").permitAll()
                .antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN)
                .and()
                .apply(securityConfigurerAdapter());

    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }
}
