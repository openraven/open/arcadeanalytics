package com.arcadeanalytics.config;

/*-
 * #%L
 * Arcade Analytics
 * %%
 * Copyright (C) 2018 - 2019 ArcadeAnalytics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.arcadeanalytics.repository.FileSystemRepository;
import io.github.jhipster.config.JHipsterProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Arcadeanalytics.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
    public static class StorageProperties {
        private String path;

        public String getPath() {
            return path;
        }

        /** The on-disk location where {@link FileSystemRepository} writes files. */
        public void setPath(final String path) {
            this.path = path;
        }
    }
    private StorageProperties storage = new StorageProperties();
    private String connectorsPath;
    private String credentialRequestHeader;
    private String principalRequestHeader;
    private String mediaPath;
    private String templateUser;

    public String getConnectorsPath() {
        return connectorsPath;
    }

    /** The on-disk location from which we will load connector jars */
    public void setConnectorsPath(final String connectorsPath) {
        this.connectorsPath = connectorsPath;
    }

    public String getCredentialRequestHeader() {
        return credentialRequestHeader;
    }

    public void setCredentialRequestHeader(final String credentialRequestHeader) {
        this.credentialRequestHeader = credentialRequestHeader;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    /** Location on disk where MediaLoader will look. */
    public void setMediaPath(final String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getPrincipalRequestHeader() {
        return principalRequestHeader;
    }

    public void setPrincipalRequestHeader(final String principalRequestHeader) {
        this.principalRequestHeader = principalRequestHeader;
    }

    public StorageProperties getStorage() {
        return storage;
    }

    public void setStorage(final StorageProperties storage) {
        this.storage = storage;
    }

    public String getTemplateUser() {
        return templateUser;
    }

    /** Identifies the username used as the template for future user creation. */
    public void setTemplateUser(final String templateUser) {
        this.templateUser = templateUser;
    }
}
